<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actividad 14 - ISSET / EMPTY</title>
</head>
<body>
    <h1>DATOS PERSONALES</h1>
    <form action="index.php" method="post">
        <label for="nombre" name="nombre" id="nombre">NOMBRE</label>
        <input type="text" name="nombre" id="nombre"> <br><br>

        <label for="edad" name="edad" id="edad">EDAD</label>
        <input type="number" name="edad" id="edad"> <br><br>

        <label for="correo" name="correo" id="correo">CORREO</label>
        <input type="email" name="correo" id="correo"> <br> <br>

        <input type="submit" value="ENVIAR INFORMACIÓN">
    </form>

    <?php

    if (isset($_POST["nombre"]) && isset($_POST["edad"]) && isset($_POST["correo"])) {
        $nombre = $_POST["nombre"];
        $edad = $_POST["edad"];
        $correo = $_POST["correo"];

        echo "NOMBRE: ", $nombre,"<br>";
        echo "EDAD: ", $edad, "<br>";
        echo "CORREO: ", $correo, "<br>";

        if (empty($nombre) || empty($edad) || empty($correo)) {
            echo "<h3>No puede haber campos vacíos.</h3>";
        }
    }

    ?>

</body>
</html>