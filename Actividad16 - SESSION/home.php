<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
    <?php
    session_start();
    ?>
    <style>
        body {
            color: <?php echo $_SESSION['color']; ?>;
        }
    </style>

    <?php
    if (!isset($_SESSION['usuario'])) {
        header('Location: index.php');
    }
    if (!isset($_SESSION['counter'])) {
        $_SESSION['counter'] = 0;
    }
    $_SESSION['counter']++;
    echo '<h1>Bienvenido, '. $_SESSION['usuario'].".</h1>";
    echo '<p>Has visitado esta página '. $_SESSION['counter'] ." veces.</p>";

    if (isset($_COOKIE['usuario'])){
        echo $_COOKIE['usuario'];
    } else {
        echo '<h3>No existe ninguna COOKIE!</h3>';
    }
    setcookie("usuario", $_SESSION['usuario'], time()+300);
    
    echo "<br><br><br>";

    //echo '<button><a href="logout.php">Cerrar sesión</a></button>';
    echo '<button type="button" class="btn btn-outline-danger"><a href="logout.php">Cerrar sesión</a></button>';
    ?>
</body>
</html>
